#include <iostream>
#include "aluno.hpp"

using namespace std;

int main(int argc, char ** argv){

    Aluno * aluno1 = new Aluno();

    aluno1->set_nome("João");
    aluno1->set_idade(20);
    aluno1->set_telefone("555-5555");
    aluno1->set_matricula(1812345);
    cout << "Aluno: " << endl;
    cout << "Nome: " << aluno1->get_nome() << endl;
    cout << "Idade: " << aluno1->get_idade() << endl;

    return 0;
}