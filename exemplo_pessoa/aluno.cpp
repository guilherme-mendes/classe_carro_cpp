#include "aluno.hpp"

Aluno::Aluno(){
    set_nome ("");
    set_telefone ("000-0000");
    set_idade (0);
    matricula = 0;
    ira = 5.0;
    semestre = 1;
}

Aluno::~Aluno(){};

int Aluno::get_matricula(){
    return matricula;
}
void Aluno::set_matricula(int matricula){
    this->matricula = matricula;
}
float Aluno::get_ira(){
    return ira;
}
void Aluno::set_ira(float ira){
    this->ira = ira;
}
int Aluno::get_semestre(){
    return semestre;
}
void Aluno::set_semestre(int semestre){
    this->semestre = semestre;
}