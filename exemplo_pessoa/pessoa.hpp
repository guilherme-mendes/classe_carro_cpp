#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std; 

class Pessoa {

	private:
		string nome;
		string telefone;
		int idade;
	public:
		Pessoa();
		~Pessoa();
		int get_idade();
		void set_idade(int idade);
		string get_nome();
		void set_nome(string nome);
		string get_telefone();
		void set_telefone(string telefone);
};

#endif
