#include "pessoa.hpp"


Pessoa::Pessoa(){
    nome = "";
    idade = 0;
    telefone = "000-0000";
}
Pessoa::~Pessoa(){}

int Pessoa::get_idade(){
    return idade;
}
void Pessoa::set_idade(int idade){
    this->idade = idade;
}
string Pessoa::get_nome(){
    return nome;
}
void Pessoa::set_nome(string nome){
    this->nome = nome;
}
string Pessoa::get_telefone(){
    return telefone;
}
void Pessoa::set_telefone(string telefone){
    this->telefone = telefone;
}